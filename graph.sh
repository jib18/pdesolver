#!/usr/bin/gnuplot
#script para gerar o gráfico dos dados de pdeSolver usando o gnuplot

reset
set grid
set datafile commentschars "#"
set xlabel "x"
set ylabel "y"
set zlabel "u(x,y)"

set hidden3d
set dgrid3d 30,30,2
set contour base
set cntrparam levels incremental 50,15,300 
splot "resultados" using 1:2:3 t"" with lines
