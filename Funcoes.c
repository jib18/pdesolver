﻿/*
*   file Funcoes.c
*   Arquivo contendo a implementação das funções utilizadas no arquivo pdeSolver.c
*   data 07/10/2019   
*      
*   Autores:
*   GRR20182962 John Israel Boldt
*   GRR20182965 João Victor Kenji Tamaki
*/

#include <stdlib.h>
//#include <sys/time.h>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include "utils.h"

#include "Funcoes.h"

#include "Funcoes.h"
#ifdef LIKWID_PERFMON
#include <likwid.h>
#else
#define LIKWID_MARKER_INIT
#define LIKWID_MARKER_START(regionTag)
#define LIKWID_MARKER_STOP(regionTag)
#define LIKWID_MARKER_CLOSE
#endif

/*!
* Funçao dada na especificação do trabalho para calcular o tempo de execução em milisegundos

double timestamp(void){

  struct timeval tp;
  gettimeofday(&tp, NULL);
  return((double)(tp.tv_sec*1000.0 + tp.tv_usec/1000.0));
} 
*/
int erros(int erro){

  switch(erro){

    case INPUT: 
        fprintf(stderr, "Erro na entrada!\n"); 
        break;
    case MALLOC: 
        fprintf(stderr, "Não foi possível alocar a memória!\n");
        break;
    case INFINITO: 
        fprintf(stderr, "Alguma operação resultou em infinito!\n");
        break;
    case FOPEN: 
        fprintf(stderr, "Não foi possível abrir o arquivo de saída!\n");
        break;
    default: 
        fprintf(stderr, "Infelizmente ocorreu um erro inesperado!\n");
        break;
  }
  return(0);
}


/*!
*   Função para liberar a estrutura de dados
*/
void liberaSistLinear(SistLinear_t * SL){

  free(SL->U);
  free(SL->r);
  free(SL);
}

/*!
* Função para alocar a estrutura de dados
*/
SistLinear_t * alocaSistLinear(int Nx, int Ny){

  int tam = Nx * Ny;
  SistLinear_t * SL = (SistLinear_t *) malloc(sizeof(SistLinear_t));
  if( SL ){

    SL->U = (real_t *) malloc (tam * sizeof(real_t));
    SL->r = (real_t *) malloc (tam * sizeof(real_t));

    if( !(SL->U) || !(SL->r)){
      	liberaSistLinear(SL);
  	  	erros(MALLOC); 
  	}else {
  		memset(SL->U, 0.0, tam*sizeof(real_t));
  		memset(SL->r, 0.0, tam*sizeof(real_t));
  	}
  }
  return(SL);
}

real_t calcula_norma(real_t * residuos, int N){

    real_t sum = 0.0;
    for(int i = 0; i < N; i++){
      sum += residuos[i] * residuos[i];
    }
    return (sum);

}

int gaussSeidel (SistLinear_t *SL, real_t * normaL2, int maxIter, real_t hx, real_t hy, int Nx, int Ny, double *tempo_medio){
    
    LIKWID_MARKER_INIT;
    LIKWID_MARKER_START("GaussSeidel");   

    double tempo_inicial = timestamp();
    double tempo_Iter = 0.0;

    real_t p2_hx = hx * hx;
    real_t p2_hy = hy * hy;
    real_t p2_PI = PI * PI; 

    real_t coef_c = 4 * p2_hx + 4 * p2_hy + 8 * p2_hx * p2_hy * p2_PI; //central
    real_t coef_d = -2 * p2_hy - p2_hy * hx; //direita
    real_t coef_e = -2 * p2_hy + p2_hy * hx; //esquerda
    real_t coef_i = -2 * p2_hx + p2_hx * hy; //inferior
    real_t coef_s = -2 * p2_hx - p2_hx * hy; //superior


    int tam = Nx * Ny;
    int j;
    int numIter = 0;
    
    double tempo_final = timestamp();
    *tempo_medio = tempo_final - tempo_inicial;
    /*!
    * Método de Gauss-Seidel
    */
        
    do{
      tempo_inicial = timestamp();
      /*!
      *cálculo das bordas
      */
      for(j = 1; j < Ny - 1; j++){
        SL->U[j] = sin(2 * PI * (PI- hx*j)) * sinh(p2_PI);
      }


      for(j = 1; j < Ny - 1; j++){
        SL->U[(Nx - 1) * Ny + j] = sin(2 * PI * j * hx) * sinh(p2_PI);
      }

      for(int i = 1; i < Nx - 1; i++){
      
        for(j = 1; j < Ny - 1; j++){
	      
          real_t f = 8 * p2_hx * p2_hy * p2_PI * (sin(2 * PI * j * hx) * sinh( PI * i * hy) + sin(2 * PI * (PI - j * hx)) * sinh(PI * (PI - i * hy)));

          /*!
          * cálculo de Uij
          */
          
          //soma as diagonais com os valores dos vizinhos de u(Xi,Yj)
          real_t d = coef_d*SL->U[(i-1)*Ny+j] + coef_s*SL->U[i*Ny+(j-1)] + coef_i*SL->U[i*Ny+(j+1)] + coef_e*SL->U[(i+1)*Ny+j];

          SL->U[i * Ny + j] = (f - d)/coef_c;

          if(SL->U[i * Nx + j] == INFINITY){ erros(INFINITO); return(1); }
        }
      }

      for(int i = 1; i < Nx - 1; i++){
      
        for(j = 1; j < Ny - 1; j++){ 
      
          real_t f = 8 * p2_hx * p2_hy * p2_PI * (sin(2 * PI * j * hx) * sinh( PI * i * hy) + sin(2 * PI * (PI - j * hx)) * sinh(PI * (PI - i * hy)));
          real_t d = coef_d*SL->U[(i-1)*Ny+j] + coef_s*SL->U[i*Ny+(j-1)] + coef_i*SL->U[i*Ny+(j+1)] + coef_e*SL->U[(i+1)*Ny+j];
          SL->r[i * Nx + j] = f - (d + coef_c * SL->U[i * Ny + j]);
        }
      }

      /*!
      * cálculo da norma L2
      */
      normaL2[numIter] = calcula_norma(SL->r, tam);
      numIter++;

      tempo_final = timestamp();
      tempo_Iter = tempo_final - tempo_inicial;
      *tempo_medio += tempo_Iter;

    }while(numIter-1 < maxIter); /// fim da iteração
    
    /*!
    * O tempo é calculado a partir do início da iteração do método até a obtenção do vetor solução daquela iteração.
    * O resultado deve ser a média aritmética do tempo de todas iterações.
    */
    *tempo_medio /= maxIter;
    LIKWID_MARKER_STOP("GaussSeidel");
    LIKWID_MARKER_CLOSE;
    return(0);
    
}

/*void imprimeResultado (SistLinear_t *SL, real_t *normaL2, int maxIter, double *tempo_medio, int Nx, int Ny, FILE *arqout){
    
    
    * Impressão no formato de comentário do Gnuplot
    
    fprintf(arqout, " ###########\n");
    fprintf(arqout, " #Tempo Método GS: %4f ms\n", *tempo_medio); 
    for (int i = 0; i < maxIter; i++){
        fprintf(arqout, " #i=%d: %10g\n", i + 1, sqrt(normaL2[i]));
    }
    fprintf(arqout, " ###########\n # \t X    \t Y    \t Z\n");

    
    *impressão do resultado como coordenadas para o Gnuplot (x,y,z)
    
    for(int i = 1; i < Nx-1; i++){
      for(int j = 1; j < Ny-1; j++){ 
       
        fprintf(arqout, "%e %e %e\n", i*PI/Nx, j*PI/Ny, SL->U[i*Ny + j]);
      }
    }
}*/

