/*!
*   file Funcoes.h
*   Arquivo contendo a declaração das funções utilizadas no arquivo pdeSolver.c
*   data 07/10/2019   
*      
*   Autores:
*   GRR20182962 John Israel Boldt
*   GRR20182965 João Victor Kenji Tamaki
*/

#ifndef __SISLINEAR_H__
#define __SISLINEAR_H__

/** Parâmetros para teste de convergência**/
#define EPS 1.0e-4
#define PI 3.1415926535897932

typedef float real_t;

typedef struct SistLinear_t{
/*!
*  - Vetor U, diagonal principal com os valores u(x,y) a serem calculados
*  - Vetor com os resíduos
*/
  real_t *U; 
  real_t *r;

} SistLinear_t;

/*!
* tipo enumerado para os possíveis erros
*/

enum erros{ INPUT = 1,
            MALLOC,
            INFINITO,
            FOPEN
          };

/*!
 *  Função erros
 *  trata os erros possíveis do programa
 */

int erros(int erro);

/*!
* função alocaSistLinear 
* aloca o sistema linear de diagonal principal U, vetor resultado b e resíduo
* retorna NULL em caso de falha
* parâmetros NX e NY definem o número de pontos no eixo x e y respectivamente
*/
SistLinear_t * alocaSistLinear (int Nx, int Ny);

/*!
* função liberaSistLinear
* libera os ponteiros do sistema em caso de falha
* parâmetro SL aponta para o sistema linear a ser solto
*/
void liberaSistLinear (SistLinear_t *SL);

/*!
* função calculo_norma
* calcula e retorna a norma L2 do resíduo do sistema linear
* parâmetro residuos passa os resíduos da última iteração
* parâmetro N passa a quantidade de pontos calculados (NX * NY)
*/
real_t calcula_norma(real_t * residuos, int N);

/*!
* função gaussSeidel
* resolve o sistema linear SL apartir do método iterativo de gauss seidel
* parâmetro SL aponta para o sistema linear
* parâmetro normaL2 aponta para o vetor onde serão guardadas as normas L2 de cada iteração
* parâmetro maxIter número máximo de iterações
* parâmetro hx e hy passa o passo no eixo x e y respectivamente* parâmetro hy passo definido em y
* parâmetro Nx e Ny passa o número de pontos no eixo x e y respectivamente
* parâmetro tempo_medio guarda o tempo medio de cada iteração
*/
int gaussSeidel (SistLinear_t *SL, real_t * normaL2, int maxIter, real_t hx, real_t hy, int Nx, int Ny, double *tempo_medio);

/*!
* função imprimeResultado
* imprime o resultado de forma a servir de entrada para o gnuplot
* parâmetro SL aponta para o sistema linear
* parâmetro normaL2 aponta para o vetor com as normas de cada iteração
* parâmetro maxIter número maximo de iterações no método de Gauss Seiel
* parâmetro tempo_medio guarda o tempo medio de cada iteração
* parâmetro hx e hy passa o passo no eixo x e y respectivamente
* parâmetro Nx e Ny passa o número de pontos no eixo x e y respectivamente
* parâmetro arqout aponta para o arquivo de saída
*/
//void imprimeResultado (SistLinear_t *SL, real_t * normaL2, int maxIter, double *tempo_medio, int Nx, int Ny, FILE *arqout);


#endif /** __SISLINEAR_H__ **/